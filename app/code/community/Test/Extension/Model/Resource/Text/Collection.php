<?php

/**
 * Class Test_Extension_Model_Resource_Collection
 */
class Test_Extension_Model_Resource_Text_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Construct collection according to configuration
     */
    protected function _construct()
    {
        $this->_init('text_entity/text');
    }


}