<?php

/**
 * Class Test_Extension_Model_Text
 */
class Test_Extension_Model_Resource_Text extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('text_entity/text', 'id');
    }

}