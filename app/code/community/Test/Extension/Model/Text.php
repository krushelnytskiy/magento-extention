<?php

/**
 * Class Test_Extension_Model_Text
 */
class Test_Extension_Model_Text extends Mage_Core_Model_Abstract
{

    /**
     * Construct model according to configuration
     */
    protected function _construct()
    {
        $this->_init('text_entity/text');
    }

}