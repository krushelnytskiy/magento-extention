<?php

/**
 * @var $this Mage_Core_Model_Resource_Setup
 */
$table = $this->getConnection()
    ->newTable($this->getTable('text_entity/text'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'unsigned' => true,
        'identity' => true,
        'nullable' => false,
        'primary' => true
    ])
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'nullable' => false,
    ])
    ->addColumn('text', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        'nullable' => false
    ])
    ->addForeignKey('product_id_foreign', 'product_id', $this->getTable('catalog/product'), 'entity_id');

$this->getConnection()->createTable($table);