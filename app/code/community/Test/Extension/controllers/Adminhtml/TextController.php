<?php

/**
 * Class Test_Extension_Adminhtml_TextController
 */
class Test_Extension_Adminhtml_TextController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Init action
     *
     * @return $this
     */
    public function init() {
        $this->loadLayout();
        $this->_setActiveMenu('text/list');

        return $this;
    }

    /**
     * Main action in adminhtml
     */
    public function indexAction()
    {
        $this->init()->renderLayout();
    }

}