<?php

/**
 * Class Test_Extension_Block_Adminhtml_List
 */
class Test_Extension_Block_Adminhtml_List extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Test_Extension_Block_Adminhtml_List constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('text_list_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('news_id', [
            'header'    => 'ID',
            'width'     => '50px',
            'index'     => 'id',
        ]);

        $this->addColumn('title', [
            'header'    => 'Product',
            'index'     => 'product_id',
            'renderer'  => Test_Extension_Block_Adminhtml_Renderer_Product::class
        ]);

        $this->addColumn('author', [
            'header'    => 'Text',
            'index'     => 'text',
        ]);

        return parent::_prepareColumns();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getModel('text_entity/text')->getResourceCollection());

        return parent::_prepareCollection();
    }

}