<?php

/**
 * Class Test_Extension_Block_Renderer_Product
 */
class Test_Extension_Block_Adminhtml_Renderer_Product extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Return values patterns
     */
    const PATTERN_LINK = '<a href="%s">%s</a>';

    /**
     * @param Varien_Object $row
     * @return mixed
     */
    public function render(Varien_Object $row)
    {
        $productId = $row->getId();

        if (($product = Mage::getModel('catalog/product')->load($productId)) instanceof Mage_Catalog_Model_Product) {

            /** @var Mage_Catalog_Model_Product $product */
            return vsprintf(
                static::PATTERN_LINK,
                [
                    $product->getUrlInStore(),
                    $product->getName()
                ]
            );
        }

        return $productId;
    }

}